---
weight: 60
title: "Study: Circular geofence alarms"
layout: bundle
---

<div style="padding: 24px ; border: 2px solid #1776BF; border-radius: 4px; margin-bottom: 24px; background-color: #f6fafe ">
  <h3 style="color: #1776BF"><strong>IMPORTANT</strong></h3>
  <p class="lead" style="font-size:22px"> The functionality described in this *CEL analytics guide* is deprecated. All new {{< product-c8y-iot >}} installations will use the Apama CEP engine. {{< company-sag >}} will terminate support for using CEL (Esper) in {{< product-c8y-iot >}} on 31 Dec 2020 following its deprecation in 2018. </p>

  <p style="font-size:16px"><strong>For further information on using Apama's Event Processing Language in {{< product-c8y-iot >}} refer to the <a href="/apama/introduction">Streaming Analytics guide</a>.</strong></p>

<p style="font-size:16px">For details on migration, refer to <a href="/apama/overview-analytics/#migrate-from-esper">Migrating from CEL (Esper) to Apama</a> in the *Streaming Analytics guide*.</p>

</div>

This section will give an in-depth example how you can create more complex rules. It will use multiple of the features explained before in the other guide sections.

If you are just starting with the {{< c8y-event-language >}} please take a look at [these examples](/event-language/examples).
