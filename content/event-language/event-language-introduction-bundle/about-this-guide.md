---
weight: 10
title: About this guide
layout: redirect
---


The *CEL analytics guide* consists of the following sections:

* Introduction
* [Data model](/event-language/data-model)
* [Functions](/event-language/functions)
* [Advanced uses cases](/event-language/advanced-cel)
* [Best practices and troubleshooting](/event-language/best-practices-cel)
* [Examples](/event-language/examples)
* [Study: Circular geofence alarms](/event-language/geofence)
